//Задача 1
//Створіть обʼєкт triangle з наступними властивостями: aSide, bSide, cSide.
//Створіть метод setValues(), який питає у користувача значення і додає їх в ключі aSide, bSide, cSide.
//Створіть в обʼєкті triangle метод для підрахунку периметра трикутника;
//Створіть в обʼекті triangle метод, який перевіряє чи є цей трикутник рівностороннім;
//Виведіть інформацію про створений трикутник (включаючи результати роботи усіх методів) в консоль.

let triangle = {
    aSide: 0,
    bSide: 0,
    cSide: 0,
    setValues: function() {
        const aSideUser = +prompt('Please enter side a:', '7');
        const bSideUser = +prompt('Please enter side b:', '7');
        const cSideUser = +prompt('Please enter side c:', '7');
        const areSidesNaN = isNaN(aSideUser) || isNaN(bSideUser) || isNaN(cSideUser);
        const areSidesNegative = aSideUser <= 0 || bSideUser <= 0 || cSideUser <= 0;

        if(areSidesNaN || areSidesNegative) {
            return 'ERROR: Please use positive numbers greater than 0 only!';
        } else {
            return `a: ${this.aSide = aSideUser}, b: ${this.bSide = bSideUser}, c: ${this.cSide = cSideUser}`;
        };
    },
    getPerimeter: function() {
        return `Perimeter of triangle is: ${(this.aSide + this.bSide + this.cSide).toFixed()}`;
    },
    isEqualSides: function() {
        return `Are triangle sides equal? ${this.aSide === this.bSide && this.bSide === this.cSide}`;
    },
};

console.log(triangle.setValues());
console.log(triangle.getPerimeter());
console.log(triangle.isEqualSides());
console.log(triangle);

//Задача 2
// Створіть обʼєкт calculator з методами:
// - read() визиває prompt() для заповнення двох значень і зберігає їх як властивості обʼєкту x, y;
// - sum() повертає суму цих двох значень;
// - multi() повертає добуток цих двох значень;
// - diff() повертає різницю цих двох значень;
// - div() повертає ділення цих двох значень;

let calculator = {
    x: 0,
    y: 0,
    read: function() {
        const xUserNumberStr = prompt('Please enter x:', '5');
        const yUserNumberStr = prompt('Please enter y:', '5');
        const xUserNumber = +xUserNumberStr;
        const yUserNumber = +yUserNumberStr;
        const areValuesNaN = isNaN(xUserNumber) || isNaN(yUserNumber);
        const areValuesEmpty = isNaN(parseFloat(xUserNumberStr)) || isNaN(parseFloat(yUserNumberStr));
        
        if(areValuesNaN || areValuesEmpty) {
            return 'ERROR: Please use numbers only!';
        } else {
            return `x = ${this.x = xUserNumber}, y = ${this.y = yUserNumber}`;
        };
    },
    sum: function() {
        return `x + y = ${(this.x + this.y).toFixed()}`;
    },
    multi: function() {
        return `x * y = ${(this.x * this.y).toFixed()}`;
    },
    diff: function() {
        return `x - y = ${(this.x - this.y).toFixed()}`;
    },
    div: function() {
        return `x / y = ${(this.x / this.y).toFixed()}`;
    },
};

console.log(calculator.read());
console.log(calculator.sum());
console.log(calculator.multi());
console.log(calculator.diff());
console.log(calculator.div());
console.log(calculator);

//Задача 3
//Даний обʼєкт country і функція format():
//Допишіть код так, щоб в консолі зʼявились рядки, які вказані в коментарях.

var country = {
    name: 'Ukraine',
    language: 'ukrainian',
    capital: {
        name: 'Kyiv',
        population: 2907817,
        area: 847.66
    }
};

function format(start, end) {
    console.log(start + this.name + end);
};

format.call(country, '', ''); // Ukraine
format.apply(country, ['[', ']']); // [Ukraine]
format.call(country.capital, '', ''); // Kyiv
format.apply(country.capital, ['', '']); // Kyiv
format.apply(window, ['']); // undefined

//Задача 4
//Що поверне даний код на екран і чому?

var text = 'outside';

function logIt() {
    console.log(text);
    var text = 'inside';
};

logIt();
/* 
відповідь - undefined, бо після виклику функції вона почне шукати на стр.114 змінну text,
а так як та оголошена через var, вона спливе догори функції logIt, але зі значенням поки що undefined, його і візьме console.log.
Вже тільки після console.log змінна text набуде значення 'inside'. До звернення до зовнішньої змінної text = 'outside' функція навіть не дійде.
*/
