//Задача 1
//Напишите функцию, которая изменяет фоновый цвет текста последнего параграфа в блоке
//<body>, а так же функцию, которая меняет блоки footer и main местами.

function changeColor(color) {
    if(typeof color === 'string') {
        const paragrToChange = document.querySelector('#main').lastElementChild;
        paragrToChange.style.backgroundColor = color;
    } else {
        console.error('Please use string for color only!');
    };
};

changeColor('#ff8db7');
//changeColor(6);

function swapBlocks() {
    const main = document.querySelector('#wrapper').lastElementChild;
    const wrapper = document.querySelector('#wrapper');
    wrapper.prepend(main);
};

swapBlocks();