//Задача 4
//(опціонально) Напишіть функцію, яка приймає аргумент number, створює матрицю у вигляді таблиці наступного типу:
//-перша ячейка у першому рядку має значення «1», а кожна наступна ячейка має значення на 1 більше;
//-перша ячейка другого рядка має значення «2» и так далі...
//Напишіть фукнцію, яка змінює колір ячейкам в таблиці, які розташовані на зворотній діагоналі матриці.

const number = 5;

function createMatrix(number) {
    new Array(number).fill('').map(function(row, trInx) {
        const trItem = document.createElement('tr');
        document.querySelector('#matrix').append(trItem);
        new Array(number).fill('').map(function(cell, tdInx) {
            const tdItem = document.createElement('td');
            tdItem.textContent = tdInx ? tdInx + trInx + 1 : trInx + 1;
            trItem.append(tdItem);
        });
    });
};

createMatrix(number);

function decorateDiagonal(number, color) {
    if(typeof color === 'string') {
        const allCells = document.querySelectorAll('td');
        const allCellsArr = [...allCells];
        allCellsArr.filter((item) => item.textContent === `${number}`)
            .map(function(item) {
                item.style.backgroundColor = color;
            });
    } else {
        console.error('Please use string for color only!');
    };
};

decorateDiagonal(number, '#fcc200');
//decorateDiagonal(number, 8);