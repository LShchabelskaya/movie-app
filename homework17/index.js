//Задача 1
//Создайте класс Worker со следующими свойствами name, surname, rate, days. 
//Напишите внутри класса метод getSalary(), который считает зарплату (рейт умноженный на количество отработанных дней) 
//и метод getInfo(), который возвращает строку с информацией о полученой зарплате сотрудника
//<name> <surname> got $<money>

//Создайте класс Boss, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker 
//и плюс дополнительное свойство totalProfit. Напишите метод getSalary(), который считает зарплату сотрудника 
//так же как метод getSalary() класса Worker + 10% от прибыли (totalProfit)
//Чтобы посчитать переменную totalProfit - нужно найти сумму зарплат всех сотрудников на позиции worker. 
//Можете найти эту переменную отдельно вне классов с помощью методов массива и передавать в класс Boss уже константой.

//Создайте класс Trainee, который наследуется от класса Worker. Этот класс имеет те же свойства, что и Worker, 
//но его метод geSalary() работает следующим образом. Во время испытательного срока (до 60 дней) сотрудник получает 70% 
//своей зарплаты, а после испытательного срока так же как обычный сотудник.

//Возьмите массив объектов представленный в файле ниже (employee.js) и создайте для каждого объекта этого массива новый объект соответсвующего класса 
//(какой класс выбрать укажет свойсвто position).
//Продемонстрируйте метод getInfo() для каждого объекта в массиве.

//Напишите задачу двумя способами (!): через классы и функцию-конструктор.

//Класс

const totalProfit = empoyees.filter((employee) => employee.position === 'worker')
                .map((employee) => employee.rate * employee.days)
                .reduce((totalProfit, currentSalary) => totalProfit + currentSalary, 0);

class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }

    getSalary() {
        return this.rate * this.days;
    }

    getInfo() {
        return `${this.name} ${this.surname} got $${this.getSalary()}`;
    }
}

class Boss extends Worker {
    constructor(name, surname, rate, days, totalProfit) {
        super(name, surname, rate, days);
        this.totalProfit = totalProfit;
    }

    getSalary() {
        const totalProfitPercent = 0.1;
        const salaryIncrease = totalProfit * totalProfitPercent;
        return +(super.getSalary() + salaryIncrease).toFixed();
    }
}

class Trainee extends Worker {
    constructor(name, surname, rate, days) {
        super(name, surname, rate, days);
    }

    getSalary() {
        const probationPeriod = 60;
        const probationRate = 0.7;
        return this.days < probationPeriod ? +(super.getSalary() * probationRate).toFixed() : super.getSalary();
    }
}

//Функция-конструктор

// function Worker(name, surname, rate, days) {
//     this.name = name;
//     this.surname = surname;
//     this.rate = rate;
//     this.days = days;
// };

// Worker.prototype.getSalary = function() {
//     return this.rate * this.days;
// };

// Worker.prototype.getInfo = function() {
//     return `${this.name} ${this.surname} got $${this.getSalary()}`;
// };

// function Boss(name, surname, rate, days, totalProfit) {
//     Worker.call(this, name, surname, rate, days);
//     this.totalProfit = totalProfit;
// };

// Boss.prototype = Object.create(Worker.prototype);
// Boss.prototype.constructor = Boss;

// Boss.prototype.getSalary = function() {
//     const getSalaryForWorker = Worker.prototype.getSalary.call(this);
//     const totalProfitPercent = 0.1;
//     const salaryIncrease = totalProfit * totalProfitPercent;
//     return +(getSalaryForWorker + salaryIncrease).toFixed();
// };

// function Trainee(name, surname, rate, days) {
//     Worker.call(this, name, surname, rate, days);
// };

// Trainee.prototype = Object.create(Worker.prototype);
// Trainee.prototype.constructor = Trainee;

// Trainee.prototype.getSalary = function() {
//     const getSalaryForWorker = Worker.prototype.getSalary.call(this);
//     const probationPeriod = 60;
//     const probationRate = 0.7;
//     return this.days < probationPeriod ? +(getSalaryForWorker * probationRate).toFixed() : getSalaryForWorker;
// };

const employeesByClasses = empoyees.map((employee) => {
    if(employee.position === 'worker') {
        return new Worker(employee.name, employee.surname, employee.rate, employee.days);
    } else if(employee.position === 'boss') {
        return new Boss(employee.name, employee.surname, employee.rate, employee.days, totalProfit);
    } else {
        return new Trainee(employee.name, employee.surname, employee.rate, employee.days);
    };
});

console.log(employeesByClasses);

for(let employee of employeesByClasses) {
    console.log(employee.getInfo());
};