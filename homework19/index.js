//Задача 1
//Создайте Promise, которое должно выполнять следующие действия:

// -через 2 секунды он должен создать переменную number со случайным значением от 1 до 6;
// -если 1 <= number <= 5, вывести в консоли сообщение "Start the game..." и вернуть число;
// -если number = 6, вернуть ошибку;

// -Затем должна быть запущена функция-потребитель (then()), которая выполняет следующие действия:

// -если number = 1, вывести сообщение "Stay here" ;
// -если number >= 2, вывести сообщение "Go <number> steps" .
// Используйте catch(), чтобы отловить ошибку и выведите сообщение "Exit" .

const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        const randomNumber = getRandomIntInclusive(1, 6);
        if(randomNumber >= 1 && randomNumber <= 5) {
            console.log('Start the game...');
            resolve(randomNumber);
        };
        reject('Exit');
    }, 2000);
});

promise
    .then((number) => {
        if(number === 1) {
            console.log('Stay here');
        } else {
            console.log(`Go ${number} steps`);
        };
    })
    .catch((error) => console.log(error));

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
};

//Задача 2
//Создайте функцию goToShop(), которая возвращает успешный Promise с количеством купленных продуктов (число взять из prompt()). 
//Создайте функцию makeDinner(), которая возвращает Promise с таймером. Через 3 секунды промис должен завершиться с текстом ‘Bon Appetit’.
//Если функция goToShop() возвращает меньше чем 4 продукта, то вернуть отклоненный промис с текстом ‘Too low products’, 
//иначе вызвать функцию makeDinner() и вывести в консоль результат работы функции.
//Если промис возвращается с ошибкой, то вывести ошибку в консоль в console.error();

function goToShop() {
    const foodstuffQuantity = +prompt('Please provide an integer positive number:', '5');
    return new Promise((resolve, reject) => resolve(foodstuffQuantity));
};

function makeDinner(quantity) {
    return new Promise((resolve, reject) => {
        if(isNaN(quantity) || quantity <= 0 || !Number.isInteger(quantity)) {
            reject('Please use positive integer numbers greater than 0!');
        } else if (quantity < 4) {
            reject('Too low products');
        } else {
            setTimeout(() => {
                console.log('Bon Appetit');
            }, 3000);
        };
    });
};

goToShop()
    .then((quantity) => {
        return makeDinner(quantity);
    })
    .catch((error) => {
        console.error(error);
    });