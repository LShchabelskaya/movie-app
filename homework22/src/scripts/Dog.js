import { Animal } from './Animal';

export class Dog extends Animal {
    constructor(nickname, food, location, weight) {
        super(nickname, food, location);
        this.weight = weight;
    }
}