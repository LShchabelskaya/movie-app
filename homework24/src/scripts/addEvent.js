import { renderEvents } from './renderEvents';
import { URL } from './constants';
import { parseHoursToMinutes } from './parser';
import { deleteBtnClickHandler } from './deleteEvent';

const addBtn = document.querySelector('#addBtn');

// const getEvents = async () => {
//     const events = await (await fetch(URL)).json(); // залишила тобі фуккцію подивитись респонс серверу
//     console.log('reCall', events); 
// };

addBtn.addEventListener('click', async (e) => {
    e.preventDefault();
    const formData = Object.fromEntries(new FormData(document.querySelector('#tasksForm')));
    const refactoredData = {
        ...formData,
        start: parseHoursToMinutes(formData.start),
        duration: +formData.duration,
        background: formData.color,
    };

    // await (await fetch(URL, {
    //     method: 'POST',                          // спроба додати правильний івент на сервер (повертається невірний обʼєкт)
    //     body: JSON.stringify(refactoredData)
    // })
    // .then(response => response.json())
    // .then(() => {
    //     getEvents();
    // }));

    document.querySelector('#form-opener').checked = false;
    renderEvents([refactoredData]);
});