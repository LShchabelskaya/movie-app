const express = require('express');
const { v4: uuidv4 } = require('uuid');
const util = require('util');
const fs = require('fs');
const bodyParser = require('body-parser');
const app = express();
const PORT = 3000;
const readFileAsPromise = util.promisify(fs.readFile);
const writeFileAsPromise = util.promisify(fs.writeFile);

app.use(bodyParser.urlencoded({
    extended: true,
}));

app.use(bodyParser.json());

app.get('/notes', async (req, res) => {
    const notes = await readFileAsPromise('./data/notes.json', 'utf-8');
    res.send(notes);
});

app.delete('/notes/:id', async (req, res) => {
    const data = await readFileAsPromise('./data/notes.json', 'utf-8');
    const notesFiltered = JSON.parse(data).filter((note) => note.id !== req.params.id);
    await writeFileAsPromise('./data/notes.json', JSON.stringify(notesFiltered));
    res.send('Success');
});

app.post('/notes', async (req, res) => {
    const note = {
        id: uuidv4(),
        dateCreated: new Date(),
        title: req.body.title,
        text: req.body.text,
        isShared: false,
    };
    const data = await readFileAsPromise('./data/notes.json', 'utf-8');
    const notes = JSON.parse(data);
    await writeFileAsPromise('./data/notes.json', JSON.stringify([...notes, note]));
    res.status(201);
    res.send('Sucess');
});

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`);
});