import { Fragment } from 'react';
import './App.css';
import Header from './Header';
import InputSong from './InputSong';
import AddButton from './AddButton';
import Songs from './Songs';
import Counter from './Counter';

function App() {
    return (
        <Fragment>
            <Header />
            <InputSong />
            <AddButton />
            <Songs />
            <Counter />
        </Fragment>
    );
}

export default App;