import { Fragment } from 'react';

function SongItem({song}) {
    return (
        <Fragment>
            <li className='item'>
                <span className='text-wrapper'>{song.name}</span>
            </li>
        </Fragment>
    );
}

export default SongItem;