import { useState } from 'react';
import './App.css';
import Header from './components/Header/Header';
import AddSong from './components/AddSong/AddSong';
import Songs from './components/Songs/Songs';
import Counter from './components/Counter/Counter';
import { songs } from './data';

function App() {
    const [songsList, setSongsList] = useState(songs);

    function addNewSong(song) {
        setSongsList([song, ...songsList]);
    };

    function deleteSong(id) {
        const newSongsList = songsList.filter(item => item.id !== id);
        setSongsList(newSongsList);
    };

    function likeSong(id) {
        const likedSongs = songsList.map(item => {
            if(id === item.id) {
                return {
                    ...item,
                    isLiked: !item.isLiked,
                };
            };
            return item;
        });
        setSongsList(likedSongs);
    };

    return (
        <>
            <Header />
            <AddSong addNewSong={addNewSong} songsList={songsList} />
            <Songs songsList={songsList} setSongsList={setSongsList} deleteSong={deleteSong} likeSong={likeSong} />
            <Counter songsList={songsList} />
        </>
    );
}

export default App;