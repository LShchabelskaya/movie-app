import React from 'react';
import './Counter.css';

function Counter({ songsList }) {
  const counter = songsList.length;

  return (
    <p className='count-title'>Count of songs:
      <span>{counter}</span>
    </p>
  );
}

export default Counter;