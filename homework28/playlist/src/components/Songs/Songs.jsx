import { useMemo } from 'react';
import SongItem from '../SongItem/SongItem';
import SortBtns from '../SortBtns/SortBtns';
import { v4 as uuidv4 } from 'uuid';
import './Songs.css';

function Songs({ songsList, deleteSong, likeSong, setSongsList }) {

    const sortedSongs = useMemo(() => {
        return [...songsList].sort(item => !item.isLiked ? 1 : -1);
    }, [songsList]);

    function sortAllSongs(isLiked) {
        if(isLiked) {
            setSongsList(sortedSongs);
        } else {
            setSongsList(sortedSongs.reverse());
        };
    };

    return (
        <div className='songs-wrapper'>
            <ul className='songs'>
                {songsList.map((song) => (
                    <SongItem song={song} key={song.id = uuidv4()} deleteSong={deleteSong} likeSong={likeSong} />
                ))}
            </ul>
            <SortBtns sortAllSongs={sortAllSongs} />
        </div>
    );
}

export default Songs;