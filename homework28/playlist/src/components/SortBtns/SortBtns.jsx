import React from 'react';
import './SortBtns.css';

function SortBtns({ sortAllSongs }) {
    return (
        <>
            <button className='sort' onClick={() => sortAllSongs(true)}>Liked</button>
            <button className='sort' onClick={() => sortAllSongs(false)}>Unliked</button>
        </>
    );
}

export default SortBtns;