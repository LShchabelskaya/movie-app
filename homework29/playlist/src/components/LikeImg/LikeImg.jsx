import React from 'react';
import like from '../../img/like.svg';
import './LikeImg.css';

function LikeImg({ song }) {
    return (
        <div>
            <img src={like} className={song.isLiked ? 'like-icon' : 'like-icon unliked'} alt='Like' />
        </div>
    );
}

export default LikeImg;