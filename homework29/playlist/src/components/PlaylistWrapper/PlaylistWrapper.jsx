import { useEffect, useContext } from 'react';
import Header from '../Header/Header';
import AddSong from '../AddSong/AddSong';
import Songs from '../Songs/Songs';
import Counter from '../Counter/Counter';
import { PlaylistContextComponent } from '../../context/Context';
import { setSongs } from '../../reducer/playlistReducer';
import { URL } from '../../constants';

function PlaylistWrapper() {
    const [, dispatch] = useContext(PlaylistContextComponent);

    useEffect(() => {
        fetch(URL)
            .then(res => res.json())
            .then(res => dispatch(setSongs(res)));
    }, [dispatch]);

    return (
        <>
            <Header />
            <AddSong />
            <Songs />
            <Counter />
        </>
    );
}

export default PlaylistWrapper;