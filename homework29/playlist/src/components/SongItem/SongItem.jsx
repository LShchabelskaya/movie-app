import { useContext } from 'react';
import LikeImg from '../LikeImg/LikeImg';
import CustomBtn from '../CustomBtn/CustomBtn';
import './SongItem.css';
import { PlaylistContextComponent } from '../../context/Context';
import { likeSong, deleteSong } from '../../reducer/playlistReducer';

function SongItem({ song }) {
    const [, dispatch] = useContext(PlaylistContextComponent);

    return (
        <li className='item'>
            <LikeImg song={song} />
            <span>{song.name}</span>
            <div className='btns-wrapper'>
                <CustomBtn 
                    className={'like'} 
                    clickHandler={() => dispatch(likeSong(song.id))} 
                    text={song.isLiked ? 'Unlike' : 'Like'} 
                />
                <CustomBtn 
                    className={'delete'} 
                    clickHandler={() => dispatch(deleteSong(song.id))} 
                    text={'Delete'} 
                />
            </div>
        </li>
    );
}

export default SongItem;