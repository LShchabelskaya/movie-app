import { useContext } from 'react';
import SongItem from '../SongItem/SongItem';
import { v4 as uuidv4 } from 'uuid';
import './Songs.css';
import CustomBtn from '../CustomBtn/CustomBtn';
import { PlaylistContextComponent } from '../../context/Context';
import { sortSongsList } from '../../reducer/playlistReducer';

function Songs() {
    const [{ songsList }, dispatch] = useContext(PlaylistContextComponent);

    return (
        <div className='songs-wrapper'>
            <ul className='songs'>
                {songsList.map((song) => (
                    <SongItem song={song} key={song.id = uuidv4()} />
                ))}
            </ul>
            <CustomBtn 
                className={'sort'} 
                clickHandler={() => dispatch(sortSongsList(true))} 
                text={'Liked'} 
            />
            <CustomBtn 
                className={'sort'} 
                clickHandler={() => dispatch(sortSongsList(false))} 
                text={'Unliked'} 
            />
        </div>
    );
}

export default Songs;