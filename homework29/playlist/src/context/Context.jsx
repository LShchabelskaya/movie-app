import { createContext, useReducer } from "react";
import { playlistReducer, initialState } from "../reducer/playlistReducer";

export const PlaylistContextComponent = createContext({});

function PlaylistContext({ children }) {
    const [ state, dispatch] = useReducer(playlistReducer, initialState);

    return (
        <PlaylistContextComponent.Provider value={[state, dispatch]}>
            {children}
        </PlaylistContextComponent.Provider>
    )
}

export default PlaylistContext;