export const initialState = {
    songsList: [],
};

const SET_SONGS = '[SONGS] Set Songs';
const ADD_SONG = '[SONG] Add Song';
const DELETE_SONG = '[SONG] Delete Song';
const LIKE_SONG = '[SONG] Like Song';
const SORT_SONGSLIST = '[SORT_SONGSLIST] Sort SongsList';

export const setSongs = (songs) => ({
    type: SET_SONGS,
    payload: songs,
});

export const addSong = (song) => ({
    type: ADD_SONG,
    payload: { song },
});

export const deleteSong = (id) => ({
    type: DELETE_SONG,
    payload: { id },
});

export const likeSong = (id) => ({
    type: LIKE_SONG,
    payload: { id },
});

export const sortSongsList = (isLiked) => ({
    type: SORT_SONGSLIST,
    payload: { isLiked },
});

export const playlistReducer = (state = initialState, action) => {
    switch(action.type) {
        case SET_SONGS:
            return {
                ...state,
                songsList: action.payload,
            };
        case ADD_SONG:
            return {
                ...state,
                songsList: [action.payload.song, ...state.songsList],
            };
        case DELETE_SONG:
            return {
                ...state,
                songsList: state.songsList.filter(item => item.id !== action.payload.id),
            };
        case LIKE_SONG:
            return {
                ...state,
                songsList: state.songsList.map(item => {
                    if (action.payload.id === item.id) {
                        return {
                            ...item,
                            isLiked: !item.isLiked,
                        };
                    };
                    return item;
                }),
            };
        case SORT_SONGSLIST:
            const sortedSongs = state.songsList.sort(item => !item.isLiked ? 1 : -1);
            if(action.payload.isLiked) {
                return {
                    ...state,
                    songsList: sortedSongs,
                };
            };
            return {
                ...state,
                songsList: sortedSongs.reverse(),
            };
        default: 
            return state;
    }
};