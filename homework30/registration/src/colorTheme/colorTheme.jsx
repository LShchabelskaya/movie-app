import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
    status: {
        danger: '#e53e3e',
    },
    palette: {
        primary: {
            main: '#ffd54f',
            darker: '#ffc107',
            contrastText: '#fff',
        },
    },
});