import TextField from '@mui/material/TextField';
import Header from '../Header/Header';
import ButtonGroup from '@mui/material/ButtonGroup';
import CustomButton from '../UI/CustomButton/CustomButton';
import SimpleDialog from '../UI/SimpleDialog/SimpleDialog';
import { inputHandler } from '../../helpers/inputHandler';
import { handleToggleModal } from '../../helpers/modalHandler';
import { useState } from 'react';
import { increaseStep, decreaseStep } from '../../reducer/LoginFormReducer';
import * as yup from 'yup';

const schemaPassword = yup.string().min(6).required();

function StepTwo({ state, dispatch }) {
    const [isValid, setIsValid] = useState({
        passwordIsValid: true,
        confirmPasswordIsValid: true,
    });

    const validateSecondStep = async () => {
        const passwordIsValid = await schemaPassword.isValid(state.password);
        const confirmPasswordIsValid = await schemaPassword.isValid(state.confirmPassword);     
        
        if (!passwordIsValid || !confirmPasswordIsValid) {
            setIsValid({
                ...isValid,
                passwordIsValid,
                confirmPasswordIsValid,
            });
        }
        else if (state.password !== state.confirmPassword) {
            handleToggleModal(dispatch);
        } else {
            dispatch(increaseStep());
            setIsValid({
                ...isValid,
                passwordIsValid: true,
                confirmPasswordIsValid: true,
            });
        };
    };

    return (
        <>
            <Header stepName={'Password'} />
            <TextField
                label='Password'
                variant='outlined'
                id='password'
                type='password'
                value={state.password}
                onChange={(e) => inputHandler(e, dispatch)}
                error={!isValid.passwordIsValid}
                helperText={isValid.passwordIsValid ? '' : '6 symbols minimum. / Can\'t be empty.'}
            />
            <TextField
                label='Confirm password'
                variant='outlined'
                id='confirmPassword'
                type='password'
                value={state.confirmPassword}
                onChange={(e) => inputHandler(e, dispatch)}
                error={!isValid.confirmPasswordIsValid}
                helperText={isValid.confirmPasswordIsValid ? '' : '6 symbols minimum. / Can\'t be empty.'}
            />
            <SimpleDialog 
                onClose={() => handleToggleModal(dispatch)} 
                open={state.isOpenModal} 
                text={'💡 These fields should coincide!'}
            >
                <ButtonGroup sx={{ justifyContent: 'center', paddingBottom: 3 }} >
                    <CustomButton
                        clickHandler={() => handleToggleModal(dispatch)}
                        text={'Try again'}
                    />
                </ButtonGroup>
            </SimpleDialog>
            <ButtonGroup sx={{ justifyContent: 'space-between' }}>
                <CustomButton
                    clickHandler={() => dispatch(decreaseStep())}
                    text={'Previous'}
                />
                <CustomButton
                    clickHandler={() => validateSecondStep()}
                    text={'Next'}
                />
            </ButtonGroup>
        </>
    );
};

export default StepTwo;