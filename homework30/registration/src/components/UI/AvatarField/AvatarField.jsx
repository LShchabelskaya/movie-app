import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';

function AvatarField( { state }) {
    return (
        <Stack sx={{ alignItems: 'center' }} >
            <Avatar
                sx={{ width: 100, height: 100 }}
                src={state.avatar.src}
                alt={state.avatar.name}
            />
        </Stack>
    );
};

export default AvatarField;