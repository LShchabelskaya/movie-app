import * as React from 'react';
import Button from '@mui/material/Button';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from '../../../colorTheme/colorTheme';

function CustomButton({ clickHandler, text }) {
    return (
        <ThemeProvider theme={theme}>
            <Button 
                sx={{width: 110, display: 'block'}} 
                variant='contained'
                color='primary' 
                onClick={clickHandler}
            >
                {text}
            </Button>
        </ThemeProvider>
    );
};

export default CustomButton;