import { toggleModal } from '../reducer/LoginFormReducer';

export const handleToggleModal = (dispatch) => {
    dispatch(toggleModal());
};