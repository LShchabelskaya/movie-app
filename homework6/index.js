//Задача 1
//Напишіть програму, яка продемонструє роботу з масивом. Створіть масив із восьми елементів:
//'455' 87.15 true undefined null 'false' [] {}
//Виведіть інформацію о типі даних кожного елемента в консолі. 
//Додайте значення 7 до кожного елементу масива і виведіть отримані значення в консолі.

const testArray = ['455', 87.15, true, undefined, null, 'false', [], {}];
const numberToAdd = 7;

for(let value of testArray) {
    if(value === null) {
        console.log('Element: ', value, 'null');
        console.log('Sum: ', value + numberToAdd);
    } else {
        const valueType = typeof value;
        console.log('Element: ', value, valueType);
        console.log('Sum: ', value + numberToAdd);
    }
}

//Задача 2
//Напишіть програму, яка питає у користувача число і створює масив numbers з випадкових цілих чисел в діапазоні від 0 до 10, 
//довжина якого дорівнює числу, яке ввів користувач.
//Виведіть створений масив numbers в консолі.
//Скопіюйте массив numbers в новий масив. Кожен третій елемент нового масиву помножте на 3.
//Виведіть новостворений масив в консолі.

const userNumberForLength = +prompt('Please enter an integer positive number for array length:', '5');
const userLengthArray = [];
const thirdIndex = 2;

if(isNaN(userNumberForLength) || userNumberForLength <= 0 || !Number.isInteger(userNumberForLength)) {
    console.error('Please use positive integer numbers greater than 0!');
  } else {
        for(let i = 0; i < userNumberForLength; i++) {
            const randomNumber = Math.round(Math.random() * 10);
            userLengthArray[i] = randomNumber;
        }
        console.log('Your array is', userLengthArray);

        const newArray = [...userLengthArray];
        for(let i = thirdIndex; i < newArray.length; i += 3) {
            newArray[i] *= 3;
        }
        console.log('Your new array is', newArray);
  }

//Задача 3
//а) Створіть массив, який складається з повних імен всіх співробітників.

const fullNames = []; // ['Karan Duffy', 'Brax Dalton', 'Jody Lam', ...]
const employeesQuantity = employee.length;

for(let i = 0; i < employeesQuantity; i++) {
    const employeeName = employee[i].name;
    const employeeSurname = employee[i].surname;
    fullNames[i] = `${employeeName} ${employeeSurname}`;
}
console.log('The list of employees full names is:', fullNames); /* не сказано вивести, але я вивела :) */

//b) Знайдіть середнє значення всіх зарплат співробітників.
//const average = /* some number */;

let salarySum = 0;

for(let value of employee) {
    salarySum += value.salary;
}

const average = salarySum / employeesQuantity;
console.log(`The average salary is ${average.toFixed(2)}`); /* не сказано вивести, але я вивела :) */

//с) Виведіть в консоль імʼя чоловіка-пільговика (ключ isPrivileges=true) 
//з самою великою зарплатою.
//const maxPrivilegesMan = /* firstName lastName */;

let maxPrivilegesMan;
let maxPrivManSalary = 0;

for(let value of employee) {
    if(value.isPrivileges && value.gender === 'male' && value.salary > maxPrivManSalary) {
        maxPrivManSalary = value.salary;
        maxPrivilegesMan = `${value.name} ${value.surname}`;
    }
}
console.log(`The man with the privilege & highest salary is ${maxPrivilegesMan}`);

//d) Виведіть в консоль повні імена (імʼя + прізвище) двох жінок з самим маленьким 
//досвідом роботи (ключ workExperience).

let minFemaleExper1 = Number.MAX_SAFE_INTEGER;
let womanFullName1;
let woman1Id = 0;
let minFemaleExper2 = Number.MAX_SAFE_INTEGER;
let womanFullName2;

for(let value of employee) {
    if(value.gender === 'female' && value.workExperience < minFemaleExper1) {
        minFemaleExper1 = value.workExperience;
        womanFullName1 = `${value.name} ${value.surname}`;
        woman1Id = value.id;
    }
}

for(let value of employee) {
    if(value.id !== woman1Id && value.gender === 'female' && value.workExperience < minFemaleExper2) {
        minFemaleExper2 = value.workExperience;
        womanFullName2 = `${value.name} ${value.surname}`;
    }
}
console.log(`The first woman is ${womanFullName1}, the second woman is ${womanFullName2}`);

//e) Виведіть в консоль інформацію, скільки всього заробили співробітники за весь час роботи в одній строці. 
//Формат відповіді: <імʼя прізвище> - <сума>.
//const result = /* your code */; // Karan Duffy - 10100
//Brax Dalton - 14400
//Jody Lam - 1440
//...

for(let value of employee) {
    const result = value.workExperience * value.salary;
    console.log(`${value.name} ${value.surname} - ${result}`);
}
